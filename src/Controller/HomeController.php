<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home.index")
     */
    public function index(ProductRepository $productRepository)
    {
        return $this->render('home/index.html.twig', [
            'mostRecentProducts' => $productRepository->findMostRecent(5),
            'cheapestProducts' => $productRepository->findCheapest(5)
        ]);
    }
}
