<?php

namespace App\Controller;

use App\Repository\CommandRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{

    private $repository;

    public function __construct(CommandRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @Route("/command", name="command.index")
     */
    public function index()
    {
        $commands = $this->repository->findAll();

        return $this->render('command/index.html.twig', [
            'commands' => $commands
        ]);
    }

    /**
     * @Route("/command/{id}", name="command.show")
     */
    public function show($id)
    {
        $command = $this->repository->find($id);

        return $this->render('command/show.html.twig', [
            'command' => $command
        ]);
    }
}
