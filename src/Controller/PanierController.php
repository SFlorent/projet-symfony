<?php

namespace App\Controller;

use App\Entity\Command;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class PanierController extends AbstractController
{

    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * @Route("/panier", name="panier.index")
     */
    public function index(SessionInterface $session, Request $request)
    {
        $command = new Command();
        $commandForm = $this->createForm(CommandType::class, $command);
        $commandForm->handleRequest($request);
    
        $panier = $session->get('panier', []);
        $products = [];
        $total = 0;
        if (isset($panier)) {
            foreach ($panier as $id => $qte) {
                $product = $this->repository->find($id);
                if (!$product) {
                    throw $this->createNotFoundException('The product does not exist');
                }
                $product->qte = $qte;
                $total += $product->getPrice();
                $command->addProduct($product);
                array_push($products, $product);
            }
        }else{
            $this->addFlash('danger', "Le panier est vide !");
        }

        if ($commandForm->isSubmitted() && $commandForm->isValid()) {   
            $command->setCreatedAt(new \DateTime);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($command);
            $entityManager->flush();
            $session->clear();
            $this->addFlash('success', "La commande a bien été passé !");
            return $this->redirectToRoute('panier.index');
        }

        return $this->render('panier/index.html.twig', [
            'products' => $products,
            'total' => $total,
            'commandForm' => $commandForm->createView() 
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add($id, SessionInterface $session)
    {
        $product = $this->repository->find($id);
        if (!$product) {
            // throw $this->createNotFoundException('The product does not exist');
            return $this->json('nok', 404);
        }

        $panier = $session->get('panier', []);
        $panier[$id] = 1;
        $session->set('panier', $panier);

        $this->addFlash('success', "Le produit {$product->getName()} a bien été ajouté au panier !");
        return $this->json('ok', 200);
    }

    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete($id, SessionInterface $session, Request $request)
    {
        $csrfToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-item', $csrfToken)) {
            $product = $this->repository->find($id);
            if (!$product) {
                throw $this->createNotFoundException('The product does not exist');
            }

            $panier = $session->get('panier', []);
            if (isset($panier)) {
                if (isset($panier[$id])) {
                    unset($panier[$id]);
                    $session->set('panier', $panier);
                    $this->addFlash('success', "Le produit {$product->getName()} a bien été retiré du panier !");
                } else {
                    $this->addFlash('danger', "Le produit n'est pas présent dans le panier !");
                }
            }else{
                $this->addFlash('danger', "Le panier est vide !");
            }
        }else{
            throw new InvalidCsrfTokenException;
        }

        return $this->redirectToRoute('panier.index');
    }
}
